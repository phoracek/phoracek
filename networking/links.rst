====================
Networking bookmarks
====================

- Great OVS tutorials http://blog.scottlowe.org/categories/#networking
- Configure OVS in libvirt xml http://www.opencloudblog.com/?p=177
- Virtual networking tutorials https://www.youtube.com/channel/UCEoaojfEY_6L5TWWjIn9t9Q
- Kernel networking http://www.linuxfoundation.org/collaborate/workgroups/networking/mainpage
- Teaming info http://www.pirko.cz/teamdev.pp.pdf
