========================
Openvswitch CLI commands
========================

.. code::

    ovs-vsctl show
    ovs-vsctl add-br <br>
    ovs-vsctl del-br <br>
    ovs-vsctl add-port <br> <port>
    ovs-vsctl del-port <br> <port>
    ovs-vsctl add-bond <br> <bond> <iface> <iface> ...

    ovs-appctl bond/show <bond>
