==================================
Openvswitch libvirt ifcfg examples
==================================

- basic info in :code:`ovs/rhel/README.RHEL`
- change network interface configuration in VM's xml to something like:

.. code:: xml

    ...
    <interface type='bridge'>
     <source bridge='br0'/>
     <virtualport type='openvswitch'/>
     <address type='pci' domain='0x0000' bus='0x00' slot='0x03' function='0x0'/>
     <vlan>
      <tag id='100'/>
     </vlan>
    </interface>
    ...


ETH-BRIDGE-VNETs
================

We are able to:

- communicate between VMs
- connect to VMs' ip from external network
- connect to host from external network

.. code::

      VM0    VM1
       |      |
     vnet0  vnet1 (vnics created by libvirt)
        \   /
         br0 (OVS bridge with IP)
          |
        eth0

    # ifcfg-br0
    DEVICE=br0
    ONBOOT=yes
    DEVICETYPE=ovs
    TYPE=OVSBridge
    OVSBOOTPROTO="dhcp"       # for dhcp
    OVSDHCPINTERFACES="eth0"  # for dhcp
    #BOOTPROTO=node           # for static IP
    #IPADDR=A.B.C.D           # for static IP
    #NETMASK=X.Y.Z.0          # for static IP
    HOTPLUG=no

    # ifcfg-eth0
    DEVICE=eth0
    ONBOOT=yes
    DEVICETYPE=ovs
    TYPE=OVSPort
    OVS_BRIDGE=br0
    BOOTPROTO=none
    HOTPLUG=no


ETH-BRIDGE-BOND-VNETs
=====================

We are able to:

- communicate between VMs
- connect to VMs' ip from external network
- connect to host from external network

.. code::

      VM0    VM1
       |      |
     vnet0  vnet1 (vnics created by libvirt)
        \   /
         br0 (OVS bridge with IP)
          |
        bond1
        /   \
      eth0  eth1

    # ifcfg-br0
    DEVICE=br0
    ONBOOT=yes
    DEVICETYPE=ovs
    TYPE=OVSBridge
    OVSBOOTPROTO="dhcp"        # for dhcp
    OVSDHCPINTERFACES="bond1"  # for dhcp
    #BOOTPROTO=none            # for static IP
    #IPADDR=A.B.C.D            # for static IP
    #NETMASK=X.Y.Z.0           # for static IP
    HOTPLUG=no

    # ifcfg-eth0 / eth1
    DEVICE=eth0  # / eth1
    ONBOOT=yes
    HOTPLUG=no

    # ifcfg-bond1
    DEVICE=bond1
    ONBOOT=yes
    DEVICETYPE=ovs
    TYPE=OVSBond
    OVS_BRIDGE=br0
    BOOTPROTO=none
    BOND_IFACES="eth0 eth1"
    OVS_OPTIONS="bond_mode=active-backup"
    HOTPLUG=no


ETH-BRIDGE-INTERNALBRIDGE(VLAN)-VNETs
=====================================

We are able to:

- connect to bridge's IP
- connect to vlan's IP
- connect to VMs' from external network (NOT TESTED)

.. code::

      VM0    VM1
       |      |
     vnet0  vnet1 (vnics created by libvirt)
        \   /
        br0.100 (internal bridge-vlan with IP)
          |
         br0 (OVS bridge with IP)
          |
        eth0

    # ifcfg-br0
    DEVICE=br0
    ONBOOT=yes
    DEVICETYPE=ovs
    TYPE=OVSBridge
    OVSBOOTPROTO="dhcp"       # for dhcp
    OVSDHCPINTERFACES="eth0"  # for dhcp
    #BOOTPROTO=none           # for static IP
    #IPADDR=A.B.C.D           # for static IP
    #NETMASK=X.Y.Z.0          # for static IP
    HOTPLUG=no

    # ifcfg-eth0
    DEVICE=eth0
    ONBOOT=yes
    DEVICETYPE=ovs
    TYPE=OVSPort
    OVS_BRIDGE=br0
    BOOTPROTO=none
    HOTPLUG=no

    # ifcfg-br0.100
    DEVICE=br0.100
    ONBOOT=yes
    DEVICETYPE=ovs
    TYPE=OVSIntPort
    #OVSBOOTPROTO="dhcp"       # for dhcp
    #OVSDHCPINTERFACES="eth0"  # for dhcp
    #BOOTPROTO=none            # for static IP
    #IPADDR=A.B.C.D            # for static IP
    #NETMASK=X.Y.Z.0           # for static IP
    OVS_BRIDGE=br0
    OVS_OPTIONS="tag=100"
    OVS_EXTRA="set Interface $DEVICE external-ids:iface-id=$(hostname -s)-$DEVICE-vif"
    HOTPLUG=no


ETH-BRIDGE-INTERNALBRIDGE(VLAN)-BOND-VNETs
==========================================

- TODO

.. code::

      VM0    VM1
       |      |
     vnet0  vnet1 (vnics created by libvirt)
        \   /
        br0.100 (internal bridge-vlan with IP)
          |
         br0 (OVS bridge with IP)
          |
        bond1
        /   \
      eth0  eth1
