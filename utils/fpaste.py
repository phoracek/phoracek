#!/bin/python3
import sys
import argparse
import json
import urllib.request
import urllib.parse

DEFAULT_USER = ''
DEFAULT_LANGUAGE = 'text'
DEFAULT_EXPIRATION = 0
DEFAULT_PROJECT = ''

CREATE_URL = 'http://fpaste.org/'
SHOW_URL = 'http://fpaste.org/show.php'


def post(url, values):
    data = urllib.parse.urlencode(values)
    data = data.encode('utf-8')
    req = urllib.request.Request(url, data)
    res = urllib.request.urlopen(req)
    code = res.getcode()
    if code == 200:
        j = res.read().decode('utf-8')
        d = json.loads(j, strict=False)
        return d['result']
    else:
        raise Exception(code)


def create(text, user, project, language, expiration, password, private):
    values = {
        'paste_data': text,
        'paste_lang': language,
        'api_submit': 'true',
        'mode': 'json'
    }
    if user != '':
        values['paste_user'] = user
    if project != '':
        values['paste_project'] = project
    if expiration != 0:
        values['paste_expire'] = expiration
    if password is not None:
        values['paste_password'] = password
    if private or password is not None:
        values['paste_private'] = 'yes'
    return post(CREATE_URL, values)


def show(id, hash, password):
    values = {
        'id': id,
        'format': 'json'
    }
    if password is not None:
        values['password'] = password
    if hash is not None:
        values['hash'] = hash
    return post(SHOW_URL, values)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description="""Usage:
  cat DOCS | ./fpaste.py --user petr
  ./fpaste.py show --id 24982 > DOCS""")
    parser.add_argument('action',
                        choices=('create', 'show'))
    parser.add_argument('--language', '-l',
                        default=DEFAULT_LANGUAGE,
                        type=str,
                        help='create: The development language used')
    parser.add_argument('--user', '-u',
                        default=DEFAULT_USER,
                        type=str,
                        help='create: An alphanumeric username of the paste '
                             'author')
    parser.add_argument('--password', '-p',
                        type=str,
                        default=None,
                        help='create: A password string to protect the paste\n'
                             'show: Password to unlock the paste (only for '
                             'protected pastes)')
    parser.add_argument('--hash',
                        type=str,
                        default=None,
                        help='show: Hash/key for the paste (only for private '
                             'pastes)')
    parser.add_argument('--id',
                        type=str,
                        help='show: ID of the paste')
    parser.add_argument('--private',
                        action='store_true',
                        help='create: Private post flag')
    parser.add_argument('--project',
                        default=DEFAULT_PROJECT,
                        help='create: Whether to associate a project with the '
                             'paste')
    parser.add_argument('--expiration', '-e',
                        default=DEFAULT_EXPIRATION,
                        help='create: Time in seconds after which paste will '
                        'be deleted from server. Set this value to 0 to '
                        'disable this feature.')
    args = parser.parse_args()

    if args.action == 'create':
        text = sys.stdin.read()
        response = create(text, args.user, args.project, args.language,
                          args.expiration, args.password, args.private)
        print("""PASTED
url: http://fpaste.org/{id}/{hash}
id: {id}
hash (if --private or --password): {hash}""".format(**response))
    elif args.action == 'show':
        response = show(args.id, args.hash, args.password)
        print(response['data'])
